import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';



@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrls: ['./show-user.component.scss']
})

export class ShowUserComponent implements OnInit {
 

  constructor(private service:SharedService) { }


  usersList:any=[];
  usersWithoutList:any=[];
  modaltitle:string | undefined;
  activateEditUsersComp:boolean=false;
  users:any;

  usersFirstNameFilter:string="";
  usersLastNameFilter:string="";
  usersEmailFilter:string="";
  usersCreatedFilter:string="";
  usersUpdateFilter:string="";

  ngOnInit(): void {
    this.refreshUsersList();
  }
  addclick(){
    this.users={
      users_id:0,
      first_name:""

    }
    this.modaltitle="Ajouter un utilisateur";
    this.activateEditUsersComp=true;
  }
  deleteClick(item: any){
    if(confirm("est bien sûr de vouloir supprimer l'exercice?")){
      this.service.deleteUsers(item.users_id).subscribe(data=>{
        this.service.deletePhoto(item.photo_file_name).subscribe(()=>{
          alert(data.toString());
          this.refreshUsersList();
        });

      })
    }
  }
  closeClick(){
    this.activateEditUsersComp=false;
    this.refreshUsersList();
  }
  editclick(item: any){
    this.users=item
    this.modaltitle="Modifier cet utilisateur";
    this.activateEditUsersComp=true;

  }

  refreshUsersList(){
    this.service.getUsersList().subscribe(data=>{this.usersList=data;this.usersWithoutList=data})
  }

  filterUsers(){
    var usersFirstNameFilter = this.usersFirstNameFilter
    var usersLastNameFilter= this.usersLastNameFilter
    var usersEmailFilter= this.usersEmailFilter
    var usersCreatedFilter= this.usersCreatedFilter
    var usersUpdateFilter= this.usersUpdateFilter
    if(usersFirstNameFilter||usersLastNameFilter||usersEmailFilter||usersCreatedFilter||usersUpdateFilter){
      this.usersList = this.usersWithoutList.filter(function(user:any){
        return user.first_name.toString().toLowerCase().includes(
          usersFirstNameFilter.toString().trim().toLowerCase()
        )&&
        user.last_name.toString().toLowerCase().includes(
          usersLastNameFilter.toString().trim().toLowerCase()
        )&&
        user.email.toString().toLowerCase().includes(
          usersEmailFilter.toString().trim().toLowerCase()
        )&&
        user.created_at.toString().toLowerCase().includes(
          usersCreatedFilter.toString().trim().toLowerCase()
        )&&
        user.updated_at.toString().toLowerCase().includes(
          usersUpdateFilter.toString().trim().toLowerCase()
        )
      })
    }else{
      this.refreshUsersList()
    }


  }
  hidePassword() {
    let hiddenPassword = "********"
    return hiddenPassword;
  }

}
