import { Component, Input, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-user',
  templateUrl: './add-edit-user.component.html',
  styleUrls: ['./add-edit-user.component.scss']
})
export class AddEditUserComponent implements OnInit {

  constructor(private service:SharedService) { }
  @Input() users:any;
  password:string | undefined;
  email:string | undefined;
  last_name:string | undefined;
  users_id:string | undefined;
  first_name:string | undefined;
  created_at:string|undefined;
  updated_at:string|undefined;
  photo_file_name:string|undefined;
  photo_file_path:string|undefined;

  ngOnInit(): void {
    this.loadeUserList()

  }

  loadeUserList(){
    this.users_id=this.users.users_id
    this.first_name=this.users.first_name
    this.last_name=this.users.last_name
    this.email=this.users.email
    this.password=this.users.password
    this.photo_file_name=this.users.photo_file_name
    if(this.photo_file_name){
    this.photo_file_path=this.service.PhotoUrl +this.users.photo_file_name
    }

  }

  addUsers(){
    var val = {
      users_id:this.users_id,
      first_name:this.first_name,
      last_name:this.last_name,
      email:this.email,
      password:this.password,
      created_at:new Date().toISOString().substring(0,10),
      updated_at:new Date().toISOString().substring(0,10),
      photo_file_name:this.photo_file_name
    };
this.service.addUsers(val).subscribe(res=>{
alert(res.toString());
});
  }

  updateUsers(){
    var val = {
      users_id:this.users_id,
      first_name:this.first_name,
      last_name:this.last_name,
      email:this.email,
      password:this.password,
      updated_at:new Date().toISOString().substring(0,10),
      photo_file_name:this.photo_file_name
    };
    this.service.updateUsers(val).subscribe(res=>{
    alert(res.toString());
    });
  }

  uploadPhoto(event:any){
    var file=event.target.files[0];
    const formData:FormData=new FormData();
    formData.append('uploadedFile',file,file.name);
    this.service.uploadPhoto(formData).subscribe((data:any)=>{
      if(this.photo_file_name){
        this.service.deletePhoto(this.photo_file_name).subscribe(()=>{
        });
      }
      this.photo_file_name=data.toString();
      this.photo_file_path=this.service.PhotoUrl+this.photo_file_name;
    })
  }

}
