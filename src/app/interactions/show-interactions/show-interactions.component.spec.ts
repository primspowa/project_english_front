import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowInteractionsComponent } from './show-interactions.component';

describe('ShowInteractionsComponent', () => {
  let component: ShowInteractionsComponent;
  let fixture: ComponentFixture<ShowInteractionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowInteractionsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowInteractionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
