import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email = '';
  password = '';

  usersList: any[] = [];

  messageErreur: string | undefined;

  constructor(private service:SharedService, private router:Router) { }
  ngOnInit(): void {
    if(sessionStorage.getItem('First name')){
      this.router.navigateByUrl('welcome')
    }
    this.getUsersList();
  }
  
  getUsersList(){
    this.service.getUsersList().subscribe(data=>{this.usersList=data;})
  }

  validate() {
    this.usersList.forEach((users: any) => {
      if (this.email === users.email && this.password === users.password) {
        this.router.navigateByUrl('welcome')
        sessionStorage.setItem('First name', users.first_name);
        sessionStorage.setItem('Last name', users.last_name);
        sessionStorage.setItem('Admin', users.admin);
      } else {
        this.messageErreur = "Email ou mot de passe incorrect";
      }
    });

  }

}
