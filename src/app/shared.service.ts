import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  readonly APIUrl = "http://127.0.0.1:8000";
  readonly PhotoUrl = "http://127.0.0.1:8000/media/"

  constructor(private http:HttpClient) {
    
   }
   
   // APPEL API DE USERS
   getUsersList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + "/speakee/users/")
  }
  addUsers(val: any){
    return this.http.post(this.APIUrl + "/speakee/users/",val)
  }
  updateUsers(val: any){
    return this.http.put(this.APIUrl + "/speakee/users/",val)
  }
  deleteUsers(val: any){
    return this.http.delete(this.APIUrl + "/speakee/users/"+val)
  }
  uploadPhoto(val: any){
    return this.http.post(this.APIUrl+"/speakee/SaveFile",val)
  }
  deletePhoto(val: any){
    return this.http.delete(this.APIUrl + "/speakee/delete/"+val)
  }


// APPEL API DE COURSES
  getCoursesList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + "/speakee/courses/")
  }
  addCoursesList(val: any){
    return this.http.post(this.APIUrl + "/speakee/courses/",val)
  }
  updateCoursesList(val: any){
    return this.http.put(this.APIUrl + "/speakee/courses/",val)
  }
  deleteCoursesList(val: any){
    return this.http.delete(this.APIUrl + "/speakee/courses/"+val)
  }

  // APPEL API DE Exercises
getExercisesList():Observable<any[]>{
  return this.http.get<any[]>(this.APIUrl + "/speakee/exercises/")
}
addExercises(val: any){
  return this.http.post(this.APIUrl + "/speakee/exercises/",val)
}
updateExercises(val: any){
  return this.http.put(this.APIUrl + "/speakee/exercises/",val)
}
deleteExercises(val: any){
  return this.http.delete(this.APIUrl + "/speakee/exercises/"+val)
}

  // APPEL API DE languages
  getLanguagesList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + "/speakee/languages/")
  }
  addLanguagesList(val: any){
    return this.http.post(this.APIUrl + "/speakee/languages/",val)
  }
  updateLanguagesList(val: any){
    return this.http.put(this.APIUrl + "/speakee/languages/",val)
  }
  deleteLanguagesList(val: any){
    return this.http.delete(this.APIUrl + "/speakee/languages/"+val)
  }
    // APPEL API DE Themes
getThemesList():Observable<any[]>{
  return this.http.get<any[]>(this.APIUrl + "/speakee/themes/")
}
addThemesList(val: any){
  return this.http.post(this.APIUrl + "/speakee/themes/",val)
}
updateThemesList(val: any){
  return this.http.put(this.APIUrl + "/speakee/themes/",val)
}
deleteThemesList(val: any){
  return this.http.delete(this.APIUrl + "/speakee/themes/"+val)
}
}
