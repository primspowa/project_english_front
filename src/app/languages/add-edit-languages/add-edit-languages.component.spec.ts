import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditLanguagesComponent } from './add-edit-languages.component';

describe('AddEditLanguagesComponent', () => {
  let component: AddEditLanguagesComponent;
  let fixture: ComponentFixture<AddEditLanguagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditLanguagesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddEditLanguagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
