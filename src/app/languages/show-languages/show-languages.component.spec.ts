import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowLanguagesComponent } from './show-languages.component';

describe('ShowLanguagesComponent', () => {
  let component: ShowLanguagesComponent;
  let fixture: ComponentFixture<ShowLanguagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowLanguagesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowLanguagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
