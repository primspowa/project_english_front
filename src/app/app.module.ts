import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {SharedService} from './shared.service'

import{HttpClientModule} from '@angular/common/http'
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { UserComponent } from './user/user.component';
import { ShowUserComponent } from './user/show-user/show-user.component';
import { AddEditUserComponent } from './user/add-edit-user/add-edit-user.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { BackComponent } from './back/back.component';
import { InteractionsComponent } from './interactions/interactions.component';
import { CoursesComponent } from './courses/courses.component';
import { ShowInteractionsComponent } from './interactions/show-interactions/show-interactions.component';
import { ShowCoursesComponent } from './courses/show-courses/show-courses.component';
import { AddEditCoursesComponent } from './courses/add-edit-courses/add-edit-courses.component';
import { LoginComponent } from './login/login.component';
import { ExercisesComponent } from './exercises/exercises.component';
import { ShowExercisesComponent } from './exercises/show-exercises/show-exercises.component';
import { AddEditExercisesComponent } from './exercises/add-edit-exercises/add-edit-exercises.component';
import { ThemesComponent } from './themes/themes.component';
import { ShowThemesComponent } from './themes/show-themes/show-themes.component';
import { AddEditThemesComponent } from './themes/add-edit-themes/add-edit-themes.component';
import { LanguagesComponent } from './languages/languages.component';
import { ShowLanguagesComponent } from './languages/show-languages/show-languages.component';
import { AddEditLanguagesComponent } from './languages/add-edit-languages/add-edit-languages.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    ShowUserComponent,
    AddEditUserComponent,
    WelcomeComponent,
    BackComponent,
    InteractionsComponent,
    CoursesComponent,
    ShowInteractionsComponent,
    ShowCoursesComponent,
    AddEditCoursesComponent,
    LoginComponent,
    ExercisesComponent,
    ShowExercisesComponent,
    AddEditExercisesComponent,
    ThemesComponent,
    ShowThemesComponent,
    AddEditThemesComponent,
    LanguagesComponent,
    ShowLanguagesComponent,
    AddEditLanguagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule 
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
