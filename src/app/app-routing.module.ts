import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoursesComponent } from './courses/courses.component';
import { ExercisesComponent } from './exercises/exercises.component';
import { LanguagesComponent } from './languages/languages.component';
import { LoginComponent } from './login/login.component';
import { ThemesComponent } from './themes/themes.component';
import { UserComponent } from './user/user.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  {path:'user',component:UserComponent},
  {path:'courses',component:CoursesComponent},
  {path:'welcome',component:WelcomeComponent},
  {path:'languages',component:LanguagesComponent},
  {path:'exercises',component:ExercisesComponent},
  {path:'themes',component:ThemesComponent},
  {path:'',component:LoginComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
