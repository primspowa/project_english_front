import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-courses',
  templateUrl: './show-courses.component.html',
  styleUrls: ['./show-courses.component.scss']
})
export class ShowCoursesComponent implements OnInit {
activateEditCoursesComp: any;
courses: any;

coursesList!: any[];
exercisesList!:any[];
themesList!: any[];
languagesList!: any[];

modaltitle: any;

coursesWithoutList: any=[];

coursesName: string="";
coursesPaid: string="";

exercisesDifficultyLevel: string="";
exercisesName: string="";

themesName: string="";

languagesName: string="";


constructor(private service:SharedService) { }

ngOnInit(): void {
  this.refreshCoursesList();
}
refreshCoursesList() {
  forkJoin({
    exercisesList: this.service.getExercisesList(),
    themesList: this.service.getThemesList(),
    languagesList: this.service.getLanguagesList(),
    coursesList: this.service.getCoursesList(),
  })
  .subscribe(({exercisesList,themesList, languagesList, coursesList}) => {
    this.exercisesList = exercisesList;
    this.themesList = themesList;
    this.languagesList = languagesList;
    this.coursesList = coursesList;
    this.coursesWithoutList = coursesList;
  });
  }

addclick() {

  this.courses={
    courses_id:0,
    name:"",
    paid:""

  }
  this.modaltitle="Ajouter un cours";
  this.activateEditCoursesComp=true;
}

deleteClick(item: any) {
  if(confirm("est bien sûr de vouloir supprimer l'exercice?")){
    this.service.deleteCoursesList(item.courses_id).subscribe(data=>{
        alert(data.toString());
        this.refreshCoursesList();
    })
  }
}
editclick(item: any) {
  this.courses=item
  this.modaltitle="Modifier le cours";
  this.activateEditCoursesComp=true;
}
filterCourses() {
  var exerciceList = this.exercisesList
  var themesList = this.themesList
  var languagesList = this.languagesList
  var coursesName = this.coursesName
  var coursesPaid= this.coursesPaid
  var exercisesDifficultyLevel = this.exercisesDifficultyLevel
  var exercisesName = this.exercisesName
  var themesName = this.themesName
  var languagesName = this.languagesName
  if(coursesName || coursesPaid || exercisesDifficultyLevel || exercisesName || themesName || languagesName){
    this.coursesList = this.coursesWithoutList.filter(function(course:any){
      return course.name.toString().toLowerCase().includes(
        coursesName.toString().trim().toLowerCase()
      )&&
      course.paid.toString().toLowerCase().includes(
        coursesPaid.toString().trim().toLowerCase()
      )
      &&
      exerciceList[course.exercises_id-1].name.toString().toLowerCase().includes(
        exercisesName.toString().trim().toLowerCase()
      )
      &&
      exerciceList[course.exercises_id-1].difficulty_level.toString().toLowerCase().includes(
        exercisesDifficultyLevel.toString().trim().toLowerCase()
      )
      &&
      languagesList[course.languages_id-1].name.toString().toLowerCase().includes(
        languagesName.toString().trim().toLowerCase()
      )
      &&
      themesList[course.themes_id-1].name.toString().toLowerCase().includes(
        themesName.toString().trim().toLowerCase()
      )
    })
  }else{
    this.refreshCoursesList()
  }

}

closeClick() {
  this.activateEditCoursesComp=false;
  this.refreshCoursesList();
}


}
