import { Component, Input, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-courses',
  templateUrl: './add-edit-courses.component.html',
  styleUrls: ['./add-edit-courses.component.scss']
})
export class AddEditCoursesComponent implements OnInit {
  @Input() courses:any;
  @Input() exercises: any;
  @Input() themes: any;
  @Input() languages: any;

  @Input() exercisesList: any[] = [];
  @Input() themesList: any[] = [];
  @Input() languagesList: any[] = [];


  courses_id: any;
  name: any;
  paid: any;

  nameExercises:any;
  difficultyLevelExercises:any
  exercises_id: any;

  nameLanguages:any;
  languages_id:any;

  namesThemes:any;
  themes_id:any;

  paidOptions = [
    { name: 'paid', value: true },
    { name: 'free', value: false },

  ]


  constructor(private service:SharedService) {
   }

  ngOnInit(): void {
    this.loadeCourseList();

  }

  loadeCourseList(){
    this.courses_id=this.courses.courses_id;
    this.name=this.courses.name;
    this.paid=this.courses.paid;
    if(this.exercises){
      this.exercises_id=this.courses.exercises_id
      this.nameExercises = this.exercises.name;
      this.difficultyLevelExercises = this.exercises.difficulty_level;
    }
    if(this.themes){
      this.themes_id=this.courses.themes_id
      this.namesThemes=this.themes.name
    }
    if(this.languages){
      this.languages_id = this.courses.languages_id
      this.nameLanguages = this.languages.name
    }


    }
    addCourses(){
      var val = {
        courses_id:this.courses_id,
        name:this.name,
        paid:this.paid,
        themes_id: this.themes_id,
        languages_id: this.languages_id,
        exercises_id: this.exercises_id,
      };
  this.service.addCoursesList(val).subscribe(res=>{
  alert(res.toString());
  });
    }
  
    updateCourses(){
      var val = {
        courses_id:this.courses.courses_id,
        name:this.name,
        paid:this.paid,
        themes_id: this.themes_id,
        languages_id: this.languages_id,
        exercises_id: this.exercises_id,
      };
      this.service.updateCoursesList(val).subscribe(res=>{
      alert(res.toString());
      });
    }

  }

 