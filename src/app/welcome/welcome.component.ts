import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  verificationAdmin:any
  themes:any
  courses:any
  exercises:any
  user:any
  messageUtilisateur:any

  constructor(private router:Router) { }

  ngOnInit(): void {
    this.creationTitreUtilisateur()
    this.verificationAdmin = sessionStorage.getItem('Admin')
  }
  logout(){
    sessionStorage.clear();
    this.router.navigateByUrl('')
  }

  creationTitreUtilisateur(){
    this.messageUtilisateur = sessionStorage.getItem('First name') + ' ' + sessionStorage.getItem('Last name') 
  }

  showsScreen(screen:any){
    if(screen=='user'){
      this.user= true
      this.exercises= false
      this.courses = false
      this.themes = false
    }
    if(screen=='exercises'){
      this.user= false
      this.exercises= true
      this.courses = false
      this.themes = false
    }
    if(screen=='courses'){
      this.user= false
      this.exercises= false
      this.courses = true
      this.themes = false
    }
    if(screen=='themes'){
      this.user= false
      this.exercises= false
      this.courses = false
      this.themes = true
    }
  }
}
