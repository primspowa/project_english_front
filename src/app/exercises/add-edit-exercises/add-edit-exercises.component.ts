import { Component, Input, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-exercises',
  templateUrl: './add-edit-exercises.component.html',
  styleUrls: ['./add-edit-exercises.component.scss']
})
export class AddEditExercisesComponent implements OnInit {

  constructor(private service:SharedService) { }
  
  @Input() exercises:any;
  exercises_id:string | undefined;
  name:string | undefined;
  difficulty_level:string | undefined;

  ngOnInit(): void {
    this.loadExercise()

  }

  loadExercise(){
    this.exercises_id=this.exercises.exercises_id
    this.name=this.exercises.name
    this.difficulty_level=this.exercises.difficulty_level
    }

    addExercises(){
      var val = {
        exercises_id:this.exercises_id,
        name:this.name,
        difficulty_level:this.difficulty_level
      };
  this.service.addExercises(val).subscribe(res=>{
  alert(res.toString());
  });
    }
    
  
    updateExercises(){
      var val = {
        exercises_id:this.exercises_id,
        name:this.name,
        difficulty_level:this.difficulty_level
      };
      this.service.updateExercises(val).subscribe(res=>{
      alert(res.toString());
      });
    }
  
}
