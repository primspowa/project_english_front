import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditExercisesComponent } from './add-edit-exercises.component';

describe('AddEditExercisesComponent', () => {
  let component: AddEditExercisesComponent;
  let fixture: ComponentFixture<AddEditExercisesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditExercisesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddEditExercisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
