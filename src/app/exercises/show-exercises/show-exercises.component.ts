import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-exercises',
  templateUrl: './show-exercises.component.html',
  styleUrls: ['./show-exercises.component.scss']
})
export class ShowExercisesComponent implements OnInit {
  exercisesDifficultyLevelFilter: any;
 

  constructor(private service:SharedService) { }


  exercisesList:any=[];
  exercisesWithoutList:any=[];
  modaltitle:string | undefined;
  activateEditExercisesComp:boolean=false;
  exercises:any;

  exercisesNameFilter:string="";

  ngOnInit(): void {
    this.refreshExercisesList();
  }
  addclick(){
    this.exercises={
      exercises_id:0,
      name:"",
      difficulty_level:""

    }
    this.modaltitle="Ajouter un exercice";
    this.activateEditExercisesComp=true;
  }

  closeClick(){
    this.activateEditExercisesComp=false;
    this.refreshExercisesList();
  }
  editclick(item: any){
    this.exercises=item
    this.modaltitle="Modifier l'exercice";
    this.activateEditExercisesComp=true;

  }

  deleteClick(item: any){
    if(confirm("est bien sûr de vouloir supprimer l'exercice?")){
      this.service.deleteExercises(item.exercises_id).subscribe(data=>{
          alert(data.toString());
          this.refreshExercisesList();
      })
    }
  }
  
  refreshExercisesList(){
    this.service.getExercisesList().subscribe(data=>{this.exercisesList=data;this.exercisesWithoutList=data})
  }

  filterExercises(){
    var exercisesNameFilter = this.exercisesNameFilter
    var exercisesDifficultyLevelFilter = this.exercisesDifficultyLevelFilter
    if(exercisesNameFilter || exercisesDifficultyLevelFilter){
      this.exercisesList = this.exercisesWithoutList.filter(function(exercises:any){
        return exercises.name.toString().toLowerCase().includes(
          exercisesNameFilter.toString().trim().toLowerCase()
        )&&
        exercises.difficulty_level.toString().toLowerCase().includes(
          exercisesDifficultyLevelFilter.toString().trim().toLowerCase()
        )
      })
    }else{
      this.refreshExercisesList()
    }
  }
  
}
