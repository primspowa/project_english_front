import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-themes',
  templateUrl: './show-themes.component.html',
  styleUrls: ['./show-themes.component.scss']
})
export class ShowThemesComponent implements OnInit {
 

  constructor(private service:SharedService) { }


  themesList:any=[];
  themesWithoutList:any=[];
  modaltitle:string | undefined;
  activateEditThemesComp:boolean=false;
  themes:any;

  themesNameFilter:string="";

  ngOnInit(): void {
    this.refreshThemesList();
  }
  addclick(){
    this.themes={
      themes_id:0,
      name:""

    }
    this.modaltitle="Ajouter un themes";
    this.activateEditThemesComp=true;
  }
  deleteClick(item: any){
    if(confirm("est bien sûr de vouloir supprimer l'exercice?")){
      this.service.deleteThemesList(item.themes_id).subscribe(data=>{
          alert(data.toString());
          this.refreshThemesList();
      })
    }
  }
  closeClick(){
    this.activateEditThemesComp=false;
    this.refreshThemesList();
  }
  editclick(item: any){
    this.themes=item
    this.modaltitle="Modifier ce themes";
    this.activateEditThemesComp=true;

  }

  refreshThemesList(){
    this.service.getThemesList().subscribe(data=>{this.themesList=data;this.themesWithoutList=data})
  }

  filterThemes(){
    var themesNameFilter = this.themesNameFilter
    if(themesNameFilter){
      this.themesList = this.themesWithoutList.filter(function(themes:any){
        return themes.name.toString().toLowerCase().includes(
          themesNameFilter.toString().trim().toLowerCase()
        )
      })
    }else{
      this.refreshThemesList()
    }


  }

}
