import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowThemesComponent } from './show-themes.component';

describe('ShowThemesComponent', () => {
  let component: ShowThemesComponent;
  let fixture: ComponentFixture<ShowThemesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowThemesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowThemesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
