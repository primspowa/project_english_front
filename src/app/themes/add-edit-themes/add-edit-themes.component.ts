import { Component, Input, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-themes',
  templateUrl: './add-edit-themes.component.html',
  styleUrls: ['./add-edit-themes.component.scss']
})
export class AddEditThemesComponent implements OnInit {

  constructor(private service:SharedService) { }
  
  @Input() themes:any;
  themes_id:string | undefined;
  name:string | undefined;

  ngOnInit(): void {
    this.loadeUserList()

  }

  loadeUserList(){
    this.themes_id=this.themes.themes_id
    this.name=this.themes.name
    }
    addThemes(){
      var val = {
        themes_id:this.themes_id,
        name:this.name,
      };
  this.service.addThemesList(val).subscribe(res=>{
  alert(res.toString());
  });
    }
  
    updateThemes(){
      var val = {
        themes_id:this.themes_id,
        name:this.name
      };
      this.service.updateThemesList(val).subscribe(res=>{
      alert(res.toString());
      });
    }
  

  }

