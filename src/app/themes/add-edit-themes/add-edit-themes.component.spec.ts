import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditThemesComponent } from './add-edit-themes.component';

describe('AddEditThemesComponent', () => {
  let component: AddEditThemesComponent;
  let fixture: ComponentFixture<AddEditThemesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditThemesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddEditThemesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
